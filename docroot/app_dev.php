<?php

use Drupal\Core\DrupalKernel;
use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request;

$autoloader = require __DIR__.'/autoload.php';

Debug::enable();
$kernel = new DrupalKernel('dev', $autoloader);

$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();

$kernel->terminate($request, $response);
